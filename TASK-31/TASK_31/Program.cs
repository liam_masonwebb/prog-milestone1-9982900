﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TASK_31
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter your number:");
            double n = double.Parse(Console.ReadLine());

            bool result = (n % 3 == 0) && (n % 4 == 0);
            Console.WriteLine("your number ( {0} ) can be divided by 3 and 4: {1}", n, result);
        }
    }
}
    
