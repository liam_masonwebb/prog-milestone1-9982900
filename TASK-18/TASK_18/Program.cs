﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TASK_18
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Tuple<int, string>> list = new List<Tuple<int, string>>();
            list.Add(new Tuple<int, string>(1998, "bob . feb"));
            list.Add(new Tuple<int, string>(1993, "mike . mar"));
            list.Add(new Tuple<int, string>(1981, "grace . dec"));


            list.Sort((a, b) => a.Item2.CompareTo(b.Item2));

            foreach (var element in list)
            {
                Console.WriteLine(element);
            }
        }
    }
}
