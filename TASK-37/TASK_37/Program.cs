﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TASK_37
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter in number of credits in the paper");

            var i = int.Parse(Console.ReadLine());
            var counter = 1;

            do
            {
                var c = ((i*10)/12);

                Console.WriteLine($" you should be studing {c} hours per week including contact hours ");
                Console.WriteLine($" you should be studing {c-5} hours per week not including contact hours ");
                i++;

            } while (i < counter);
        }
    }
}