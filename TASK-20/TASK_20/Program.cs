﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TASK_20
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("even numbers are");
            int[] values = { 34, 44, 86 };
            Array.Sort(values);
            foreach (int value in values)
            {
                Console.Write(value);
                Console.Write(' ');
            }
            Console.WriteLine();
        }
    }
}