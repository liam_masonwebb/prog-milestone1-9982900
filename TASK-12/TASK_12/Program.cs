﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TASK_12
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please write your number:");
            int n = int.Parse(Console.ReadLine());

            bool result = n % 2 == 0;
            Console.WriteLine("Is Your number {0} even? Answer: {1}", n, result);
        }
    }
}

