﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TASK_05
{
    class program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter in 24 hour time between 12 and 24... ( in 2 number format e.g 15)");

            var i = int.Parse(Console.ReadLine());
            var counter = 1;

            do
            {
                var c = i - 12;

                Console.WriteLine($" {c} PM ");

                i++;

            } while (i < counter);
        }
    }
}
