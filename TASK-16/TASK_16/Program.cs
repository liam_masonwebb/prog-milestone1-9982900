﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TASK_16
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("please type word");
            string temp = Console.ReadLine();
            int letters = 0;

            foreach (char c in temp)
            {
                if (char.IsLetter(c))
                {
                    letters += 1;
                }
            }

            Console.WriteLine("your word has a character count of: " + letters);
            Console.ReadLine();
        }
    }
}
 