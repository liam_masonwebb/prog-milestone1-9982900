﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TASK_17
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Tuple<int, string>> list = new List<Tuple<int, string>>();
            list.Add(new Tuple<int, string>(18, "bob"));
            list.Add(new Tuple<int, string>(17, "mike"));
            list.Add(new Tuple<int, string>(24, "grace"));


            list.Sort((a, b) => a.Item2.CompareTo(b.Item2));

            foreach (var element in list)
            {
                Console.WriteLine(element);
            }
        }
    }
}
