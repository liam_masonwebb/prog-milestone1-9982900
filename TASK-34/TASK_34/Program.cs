﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TASK_34
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter in weeks");

            var i = int.Parse(Console.ReadLine());
            var counter = 1;

            do
            {
                var c = ((i*7)/7)*5;

                Console.WriteLine($" {c} week days ");

                i++;

            } while (i < counter);
        }
    }
}

