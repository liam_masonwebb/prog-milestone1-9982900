﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TASK_28
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter three words including spaces ['' '' '']");

            string userInput = Console.ReadLine();
            string[] words = userInput.Split(' ');
            if (words.Length >= 3)
            {
                string one = words[0];
                Console.WriteLine(one);
                string two = words[1];
                Console.WriteLine(two);
                string three = words[2];
                Console.WriteLine(three);

            }
            else
            {
                Console.WriteLine("incorrect number of words.");
            }
        }
    }
}