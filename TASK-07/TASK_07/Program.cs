﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TASK_07
{
    class program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter in number...");

            var i = int.Parse(Console.ReadLine());
            var counter = 1;

            do
            {
                var c = i * 1;
                var a = i * 2;
                var s = i * 3;
                var d = i * 4;
                var f = i * 5;
                var g = i * 6;
                var h = i * 7;
                var j = i * 8;
                var k = i * 9;
                var l = i * 10;
                var z = i * 11;
                var x = i * 12;

                Console.WriteLine($" {i} x 1 = {c}");
                Console.WriteLine($"{i} x 2 = {a}");
                Console.WriteLine($"{i} x 3 = {s}");
                Console.WriteLine($"{i} x 4 = {d}");
                Console.WriteLine($"{i} x 5 = {f}");
                Console.WriteLine($"{i} x 6 = {g}");
                Console.WriteLine($"{i} x 7 = {h}");
                Console.WriteLine($"{i} x 8 = {j}");
                Console.WriteLine($"{i} x 9 = {k}");
                Console.WriteLine($"{i} x 10 = {l}");
                Console.WriteLine($"{i} x 11 = {z}");
                Console.WriteLine($"{i} x 12 = {x}");
                i++;

            } while (i < counter);
        }
    }
}
